# part 1

typeof(mtcars) # "list"
is.data.frame(mtcars) # TRUE

typeof(mtcars[,2]) # "double"
is.vector(mtcars[,2]) # TRUE

mtcars["Fiat 128", "cyl"] # 4
mtcars[mtcars[,"cyl"] == mtcars["Fiat 128", "cyl"],]

min(mtcars[,"cyl"]) # 4
mtcars[mtcars[,"cyl"] == min(mtcars[,"cyl"]),]

correlation = cor(mtcars)
typeof(correlation) # "double"
is.matrix(correlation) # TRUE

lowCorrelation = correlation[1, correlation[1,] < -0.7]

# part 2

values = rnorm(100, 40, 10)
thirdValues = values[1:33 * 3]
exceptFifth = values[-(1:20 * 5)]
onlyEven = values[trunc(values) %% 2 == 0]

root =
  list(
    left = list(
      left = "a",
      right = list(
        left = "b",
        right = "c"
      )),
    right = list(
      left = "d",
      right = "e"
    )
  )

nodes = unlist(root, use.names = F) # "a", "b", "c", "d", "e"
leftNode = root$left
bcNode = leftNode$right
