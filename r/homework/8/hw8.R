rnaSeq = read.table("hw6.counts.txt")

rnaSeq.Norm.Lib = apply(rnaSeq, 2, function(x) x/sum(x) * 10**6)
rnaSeq.Norm.Expr = t(apply(rnaSeq.Norm.Lib, 1, function(x) (x - mean(x)) / sd(x)))
# transposing due to the way apply() joins results
rnaSeq.Norm.Expr[is.nan(rnaSeq.Norm.Expr)] = 0

rnaSeq.PCA = prcomp(t(rnaSeq.Norm.Expr))$x

correlations = cor(rnaSeq.Norm.Expr, use = "pair", method = "p")
rnaSeq.MDS = cmdscale(1-correlations, k=4)

brains = startsWith(colnames(rnaSeq.Norm.Expr), "b")

# Pch

pch = numeric(ncol(rnaSeq.Norm.Expr))
pch[brains] = 16
pch[!brains] = 18

get.Age = function(cname) {
  position = regexpr(pattern = "[0-9]{2}(\\.5)?", cname, perl = TRUE)
  startIndex = position
  endIndex = startIndex + attr(position, "match.length") - 1
  as.numeric(substr(cname, startIndex, endIndex))
}

ages = sapply(colnames(rnaSeq.Norm.Expr), get.Age)

# Colors, min = 55, max = 255

brain.Colors = ages[brains]
max.BrainAge = max(brain.Colors)
step = 200 / max.BrainAge
brain.Colors = sapply(brain.Colors, function (x) rgb(55 + floor(step * x), 0, 0, 192, maxColorValue = 255))

liver.Colors = ages[!brains]
max.LiverAge = max(liver.Colors)
step = 200 / max.LiverAge
liver.Colors = sapply(liver.Colors, function (x) rgb(0, 0, 55 + floor(step * x), 192, maxColorValue = 255))

colors = c(brain.Colors, liver.Colors)

# Point size, min = 1, max = 2.5

brains.Cex = ages[brains]
min.BrainAge = min(brains.Cex)
step = (max.BrainAge - min.BrainAge) / 1.5
brains.Cex = sapply(brains.Cex, function (x) ((x - min.BrainAge) / step) + 1)

livers.Cex = ages[!brains]
min.LiverAge = min(livers.Cex)
step = (max.LiverAge - min.LiverAge) / 1.5
livers.Cex = sapply(livers.Cex, function (x) ((x - min.LiverAge) / step) + 1)

cex = c(brains.Cex, livers.Cex)

par (mfrow = c(2, 2))

plot(rnaSeq.PCA[, "PC1"], rnaSeq.PCA[, "PC2"], pch = pch, cex = cex, col = colors)
plot(rnaSeq.PCA[, "PC3"], rnaSeq.PCA[, "PC4"], pch = pch, cex = cex, col = colors)
plot(rnaSeq.MDS[, 1], rnaSeq.MDS[, 2], pch = pch, cex = cex, col = colors)
plot(rnaSeq.MDS[, 3], rnaSeq.MDS[, 4], pch = pch, cex = cex, col = colors)

heatmap(correlations,col=rev(heat.colors(100)),
        distfun=function(x){as.dist(1-x)},
        symm=T, ColSideColors=colors)

fdr.Vector = numeric(200)
for (i in 1:200) {
  indices = sample(1:113, 113)
  for (j in 1:nrow(rnaSeq.Norm.Expr)) {
    a = rnaSeq.Norm.Expr[j, indices[1:56]]
    b = rnaSeq.Norm.Expr[j, indices[57:113]]
    pvalue = t.test(a, b)$p.value
    if (!is.na(pvalue) & pvalue < 0.05) {
      fdr.Vector[i] = fdr.Vector[i] + 1
    }
  }
}

pvalues = as.vector(apply(rnaSeq.Norm.Expr, 1, function(x) t.test(x[1:56], x[57:113])$p.value))
pvalues.NonNan = pvalues[!is.nan(pvalues)]
pvalues.Adjusted = p.adjust(pvalues.NonNan, method = "BH")
pvalues.Closest = max(pvalues.Adjusted[pvalues.Adjusted < 0.05])
pvalue.New = pvalues.NonNan[which(pvalues.Adjusted == pvalues.Closest)]
# pvalue.New = 0.03820295

fdr = mean(fdr.Vector) / nrow(rnaSeq.Norm.Expr)
# 0.04217447
