drawregression <- function(a, b) {
  plot(a, b)
  abline(lm(b ~ a), col = 'red')
}

geneticData = read.table('hw1.txt')
cpy14 = geneticData$CPY14
rmd4 = geneticData$RMD4
htvwq = geneticData$htVWQ

cor(cpy14, rmd4) # 0.8303976
cor.test(cpy14, rmd4, m = 'p')$p.value # 1.232801e-26

cor(cpy14, htvwq) #  -0.1265428
cor.test(cpy14, htvwq, m = 'p')$p.value # 0.2096353

cor(rmd4, htvwq) #  -0.1332689
cor.test(rmd4, htvwq, m = 'p')$p.value # 0.1862

# there's correlation between CPY14 and RMD4 expressions

wilcox.test(cpy14, rmd4)$p.value # 0.009496109
wilcox.test(cpy14, htvwq)$p.value # 0.0308708
wilcox.test(rmd4, htvwq)$p.value # 2.65261e-05

# all three pairs has diffrent means:
# CPY14 and RMD4, CPY14 and htVWQ, RMD4 and htVWQ

# drawing plots

par(mfrow=c(2, 3))

drawregression(cpy14, rmd4)
drawregression(cpy14, htvwq)
drawregression(rmd4, htvwq)

boxplot(cpy14, rmd4)
boxplot(cpy14, htvwq)
boxplot(rmd4, htvwq)