sampleSize = 10000

xcoord = runif(sampleSize, -1, 1)
ycoord = runif(sampleSize, -1, 1)

whichCount = length(which((xcoord * xcoord + ycoord * ycoord) <= 1))

isInside = function(x, y) {
  return ((x * x + y * y) <= 1)
}

loopCount = 0
for (i in 1:sampleSize) {
  x = runif(1, -1, 1)
  y = runif(1, -1, 1)
  
  if (isInside(x, y)) {
    loopCount = loopCount + 1
  }
}

whichProbability = whichCount / sampleSize # 0.7831
loopProbability = loopCount / sampleSize   # 0.7898

whichPiValue = whichCount / sampleSize * 4  # 3.1324
loopPiValue = loopCount / sampleSize * 4    # 3.1592

getPiEstimate = function(s) {
  xc = runif(s, -1, 1)
  yc = runif(s, -1, 1)
  
  count = length(which((xc * xc + yc * yc) <= 1))
  piValue = count / s * 4
  return (abs(piValue - pi))
}

samples = 0:150 * 1000
samples[1] = 100
estimates = sapply(samples, getPiEstimate)
plot(samples, estimates, t='l', log = 'y')
