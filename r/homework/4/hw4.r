random.Text = function(text.Length, alphabet) {
  alphabet.AsArray = unlist(strsplit(alphabet, ""))
  alphabet.Length = length(alphabet.AsArray)
  if ((text.Length < 0) || alphabet.Length <= 0)
    return ("");
  
  indices = round(runif(text.Length, 1, alphabet.Length));
  return (paste(alphabet.AsArray[indices], collapse = ""))
}
random.Text(-1, "ATGC") # ""
random.Text(10, "") # ""
random.Text(8, "ATGC") # "CGAAGAGG"

count.Subtexts = function(text, subtext.Length) {
  if ((typeof(text) != "character") || (typeof(subtext.Length) != "double") ||
      (subtext.Length <= 0) || (trunc(subtext.Length) - subtext.Length != 0))
    return (NA)
  
  text.Length = nchar(text)
  last.Substring.Index = text.Length - subtext.Length + 1
  
  if (text.Length < subtext.Length)
    return (NA)
  
  all.Subtexts = substring(text, 1:last.Substring.Index, subtext.Length:text.Length)
  unique.Subtexts = unique(all.Subtexts)
  counts = sapply(unique.Subtexts, function(subtext) length(all.Subtexts[all.Subtexts == subtext]))
  
  data.frame(unique.Subtexts, counts)
}
count.Subtexts("abbbca", 2) # (ab = 1, bb = 2, bc = 1, ca = 1)
count.Subtexts("abbbca", 6) # (abbbca = 1)

install.packages("ape")
library("ape")

dna = paste(read.dna("sequence.fasta", format="fasta", as.character=TRUE), collapse = "")
subtexts.LengthSix = count.Subtexts(dna, 6)
dna.Subtexts.Length = nrow(subtexts.LengthSix)
subtexts.Ordered = subtexts.LengthSix[order(subtexts.LengthSix["counts"]),]
subtexts.Ordered[1, ] # "cctagg" 24
subtexts.Ordered[2, ] # "ctagac" 25
subtexts.Ordered[dna.Subtexts.Length - 1, ] # "ctggcg" 5666
subtexts.Ordered[dna.Subtexts.Length, ] # "cgccag" 5883
