l = list(a=1:10, b = -1:3, c=200:301)
lapply(l, mean)
sapply(l, mean)
l$b[2] = NA
sapply(l, summary)
t(sapply(l, summary))
mean(l$b, na.rm=T)
mean(l$b, trim = 0.1)
sapply(1:10, max)

sapply(l, '[', 1)
sapply(l, function(x){x[seq(1, by=3, to=length(x))]})
sapply(l, '[', c(TRUE, FALSE, FALSE))

sapply(l, function(x){prod(abs(x))^(1/length(x))})

m = matrix(rnorm(100, 10+rep(1:10, each=10)), ncol=10)
apply(m, 2, mean)
sweep(m, 2, nf, '/')
# scale

do.call(plot, list(x=1:10, y=sin(1:10)))
t = apply(round(m), 1, paste, collapse=',')
m = do.call(rbind, lapply(strsplit(t, ','), as.numeric))
as.numeric(m)
m

ifelse(runif(10)>0.5, paste0('s', 1:10), 'f')
sex = ifelse(runif(20)>0.5, 'm', 'f')
height = ifelse(sex=='m', rnorm(20, 182), rnorm(20, 172))
height[sex=='m']
sh = split(height, sex)
boxplot(sh)
boxplot(height ~ sex)
sapply(sh, mean)
data = data.frame(sex=sex, height=height, age=rnorm(length(sex), 35))
data
t = split(data, data$sex)
sapply(t, function(x)x[which.max(x$height),])
# do.call(rbind, sapply(t, function(x)x[which.max(x$height),]))
split.data.frame(as.matrix(data), data$sex)

install.packages('plyr') 
library(plyr) 
?plyr
laply(baseball, typeof, .progress = 'text')

# install.packages('doMC')
# library(doMC)
registerDoMC(2) # кол-во потоков
laply(baseball, typeof, .progress = 'text', .parallel = TRUE)
l_ply(1:10, function(i)mean(rnorm(1e6)), .progress = 'text')
