print ('Hello world!');
d = rnorm(300)
plot(d)
d
d[1:10]
summary(d)
hist(d)
den = density(d)
plot(den, ylim = c(0, max(y, den$y)))
?rnorm
x = seq(-3, 3,length.out = 100)
y = dnorm(x)
den = density(d)
lines (x, y, col = 'red')

y = rnorm(length(d), mean = d + 1, sd = abs(d))
plot(d, y)
dnorm(300)
dnorm(300, log = T)

d = cbind(d, y)
d
d[1:2,]
d[1:4,]
d[1:4,1]
plot(d[,1],d[,2], xlab = 'original data', ylab = 'new data')
abline(lm(d[,2] ~ d[,1]), col = 'red')
par(mfrow=c(2, 2))
boxplot(d)
hist(d[,1])
hist(d[,2])
par(mfrow=c(1, 1))
dev.off()
plot(density(d[,2]))
lines(density(d[,1]), col='red')
mean(d[,1])
mean(d[,2])

t.test(d[,1], d[,2])$p.value
wilcox.test(d[,1],d[,2])$p.value

####
d[,1] > 0
d[,2] > 0
table(d[,1] > 0)
table(d[,2] > 0)
table(first = d[,1] > 0, second = d[,2] > 0)
fisher.test(table(first = d[,1] > 0), table(second = d[,2] > 0))
cor(d[,1], d[,2])
cor(d[,1], d[,2], m = 's')
cor(d[,1], d[,2], m = 'p')
cor(1:2,2:3)
cor.test(d[,1], d[,2], m = 'p')$p.value
cor.test(1:3,2:4,m = 'p')$p.value
cor.test(d[,1],d[,2], m = 'sp')

####
options(digits = 20)
1/3

####
getwd()
setwd("C:")
write.table(d)
write.csv(d)
write.table(d, file = 'test.txt', sep='\t')
ls()
rm(d)
d
d = read.table('test.txt')
d
save.image('all.RData')
rm(list = ls())
ls()
ls()
load('all.RDATA')
save(d, x, file = 'a.save')
saveRDS(d, 'd.RDATA')


new.d = readRDS('d.RDATA')
set.seed(1)
rnorm(1)
rnorm(1)
set.seed(1)
rnorm(1)

####
pdf('image1.pdf', width = 6, height = 6)
plot(d)
dev.off()
