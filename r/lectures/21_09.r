x = list(1, 3, 'a', c(T, T, F), c(2, 3, 1))
str(x)

x = list(list(1, 2), 3, 4)
str(x)
y = c(list(1, 2), 3, 4)
str(y)

unlisrt(x)

###

x = list(int=-1:3, char='a', var3=c(T, F, T), var4=c(2, 3, 1))
x[1:2]
x['var3']
x[c(T, F, T, F)]

x[[1]]
x$var3

######
x = matrix(1:12, ncol=3)
y = matrix(1, ncol=3, nrow=6)
dim(y)


x = 1:10
attr(x, 'dim') = c(5,2)

rownames(x) = c('r1', 'r2', 'r3', 'r4', 'r5')
colnames(x) = c('c1', 'c2')
dimnames(x)

###
x[4]

###
x[1:2,]
x[1:2,c(2,1)]

x[c('r1', 'r2'), c('c1', 'c2')]
x[x[,2] < 6,]
y = x[1,,drop=FALSE]
dim(y)

y = x[1]
dim(y)

x > 4
x < 5

x / 1:2
which(x > 5, arr.ind = T)

x = matrix(NA, ncol = 5, nrow = 5)
x = rbind(x, 1:5)
x = rbind(x, x)
x = cbind(x, NA)

###
x = data.frame(a = 1:3, b=c('a', 'b', 'c'))
cbind(a = 1:3, b=c('a', 'b', 'c')) # everything converted to string

dim(x)
ncol(x)
nrow(x)
colnames(x)
rownames(x) = c('r1', 'r2', 'r3') # only rownames MUST have unique values

x['r1',]
# data.frame is a list with column names

x$a
x

###
options(stringsAsFactors = FALSE)
