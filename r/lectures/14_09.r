TRUE
FALSE

# the three following operands are equivalent
a <- TRUE
TRUE -> a
a = TRUE

a = NA # not available, e.g. not filled in table
a = -1/0 # inf
a = log(-1) # NaN
NULL # no data
x = 1:10
x[1] = NA
x[1] = NULL # error

a = 1
a = 'a'
a = "a"
new.var1 = 1

a = c(T, T)
length(a)
typeof(a) # 'logical'

a = c(1L, 1L)
typeof(a) # 'integer'

d = c(1,1,1,1e-10)
typeof(d) # 'double'

s = c('a', 'b')
typeof(s) # 'character'

c(1,1,3,c(2,1))
a = 1:2
b = 1.2:7
c = c(a, b)

c(c+10)

typeof(c(1,F,1L))

T+F # 1
T + 1.1 # 2.1
1 & 1 # TRUE
1 & 0 # FALSE
1 + '10' # error

as.integer('10') # 10
as.double(T) # 1
as.logical('TRUE') # TRUE
as.character(100000) # 1e+05


f = factor(c('f', 'm', 'f'))
f

f = factor(c('f', 'f', 'f'), levels = c('f', 'm'))
f           

table(f)
table(c('f', 'f', 'f'))
typeof(f)
length(f)

x = 1:10
attr(x, 'my.attr') = 'new attr'
attr(x, 'names') = paste0('name', 1:10)

names(x)
names(x)[1] = 'new.name'
x = c(a=1, b=4, c=-1, d=-5,d=10)
names(x)

###
x[2:3]
x[c(1,5,1)]
x[c('a', 'a', 'd')]
x[c('d')]
table(table(names(x)))
x[x < 0]
x[T]
x[c(T, F)]
which(x < 0)

###

x = runif(100, 0, 5)
sort(x)

y = sin(x)
plot(x, y, t='l')
o = order(x)
plot(x[o], y[o], t = 'l')

###

10 * 2
10 ** 2
power(10, 2)

10 %% 3
10 / 3

10 != 2

T || F
c(T, T) || c(T, F)

1:10 + 1:5 # 1:5 will be replicated twice
1:10 + 3

sum(1:10 != 1:10)

i = 0
while(i < 4) {
  print(i)
  i = i + 1
}

for (i in 1:10 )
{
  cat(i)
}

a = 1
a[2] = 
  
sum(1:10 != 1:10)

a = character(10)

a = numeric(6000000)
system.time(for(i in 1:6000000) (a[i] = i))

a = numeric(0)
system.time(for(i in 1:6000000) (a[i] = i))
