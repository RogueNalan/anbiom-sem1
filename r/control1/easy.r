compare.Words = function(a, b) {
  a.Array = strsplit(a, "")
  b.Array = strsplit(b, "")
  
  ab.Frame = data.frame(a.Array, b.Array)
  compared = apply(ab.Frame, 1, function(c) c[1] == c[2])
  
  length(compared[compared == FALSE]) / nchar (a)
}

get.Comparison = function(words) {
  words.Count = length(words)
  result = matrix(1:(words.Count * words.Count), nrow = words.Count)
  rownames(result) = words
  colnames(result) = words
  
  for (i in 1:words.Count) {
    for (j in 1:words.Count) {
      result[i, j] = compare.Words(words[i], words[j])
    }
  }
  
  result
}

abc = get.Comparison(c("abc", "abc"))