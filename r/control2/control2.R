genes = read.csv("8.tab", sep = " ")
genes[, 6] = as.factor(genes[, 6])

model = lm(log2fc ~ ., genes)

result = anova(model)$`Pr(>F)`[1:5]
names(result) = colnames(genes)[2:6]
result

# tataat.seq         first.nt  after.tataat.AT before.tataat.AT         site.pos 
# 1.285833e-13     6.621479e-12     6.568588e-01     7.775183e-06     3.757875e-06

# tataat.seq, first.nt, before.tataat.AT, site.pos have p-value less than 0.05


get.Coeffcients = function(coefs, col.Name) {
  coefs.Names = names(coefs)
  indices = which(startsWith(coefs.Names, col.Name))
  col.Name.Length = nchar(col.Name)
  
  value.Names = sapply(indices,
    function (i) substring(coefs.Names[i], col.Name.Length + 1, nchar(coefs.Names[i])))
  values = sapply(indices, function (i) coefs[i] > 0)
  cbind(value.Names, values)
}

tataat.seq.Coefs = get.Coeffcients(model$coefficients, "tataat.seq")
tataat.seq.Names = genes[, 2]
(tataat.seq.Names[!(tataat.seq.Names %in% tataat.seq.Coefs[, 1])])[1]
# All values except "TATACT" increase gene expression
# "CATAAT" coefficient is 0

first.Nt.Coefs = get.Coeffcients(model$coefficients, "first.nt")
# All values except T increase expression
# "A" coefficient is 0

site.pos.Coefs = get.Coeffcients(model$coefficients, "site.pos")
site.pos = genes[, 6]
# Values 2 and 3 decrease expression
# Value 1 coefficient is 0

before.tataat.AT.Coefs = get.Coeffcients(model$coefficients, "before.tataat.AT")
# The after.tataat.AT has p-value > 0.05
# The before.tataat.AT has p-value < 0.05 and decreases the expression

predicted = sapply(1:nrow(genes), function (i) {
  model = lm(log2fc ~ tataat.seq + first.nt + before.tataat.AT + site.pos, genes[-i, ])
  predict(model, genes[i, c(2, 3, 5, 6)])
})

plot(genes[, 1], predicted)
cor(genes[, 1], predicted)

# 0.3498