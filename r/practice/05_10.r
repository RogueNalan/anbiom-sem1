fact = function(x) {
  if (((floor(x) - x) != 0) || (x < 0))
    return (NA);
  
  if (x == 0)
    return (1);

  return (Reduce(`*`, 1:x));
}

harmonic.mean = function(x) {
  harmonic.sum = sum(x ^ -1) / length(x);
  return (harmonic.sum ^ -1);
}
harmonic.means = apply(mtcars, 2, harmonic.mean)

my.polynom = function(x, coefs) {
  xPowers = x ^ ((length(coefs) - 1):0);
  return (sum(coefs * xPowers));
}

my.polynom.factory = function(coefs) {
  return (function(x) my.polynom(x, coefs));
}

a = my.polynom.factory(c(1, 2, 3))
a(3)
