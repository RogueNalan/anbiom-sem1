genes = read.table("counts5.txt")
nonZero = apply(genes, 1, function(x) sum(x[1:4]) > 0 & sum(x[5:8]) > 0)
genes = genes[nonZero, ]

sum.Genes = genes[, 1] + genes[, 2] + genes[, 3] + genes[, 4]
sum.Genes.H2O = genes[, 5] + genes[, 6] + genes[, 7] + genes[, 8]

new.Genes = cbind(sum.Genes, sum.Genes.H2O)

all.Reads = sum(new.Genes[, 1])
all.Reads.H2O = sum(new.Genes[, 2])

pvalues = apply(new.Genes, 1, function(x) fisher.test(matrix(c(
  x[1],
  x[2],
  all.Reads - x[1],
  all.Reads.H2O - x[2]
), ncol = 2))$p.value)

new.Genes = cbind(new.Genes, pvalues)
original.Genes.Count = sum(new.Genes[, "pvalues"] < 0.05) # 3003

pvalues.Adjusted = p.adjust(pvalues, method = "BH")
pvalue.Closest = max(pvalues.Adjusted[pvalues.Adjusted < 0.05])
pvalue.Closest.Index = which(pvalues.Adjusted == pvalue.Closest)
pvalue.New = pvalues[pvalue.Closest.Index]

adjusted.Genes.Count = sum(new.Genes[, "pvalues"] < pvalue.New) # 1708

results = 1:10 * 0
for (i in 1:10) {
  samples = sample(1:8, 8)
  perm.Table = cbind(genes[samples[1:4]], genes[samples[5:8]])
  
  perm.Sum.Genes = perm.Table[, 1] + perm.Table[, 2] + perm.Table[, 3] + perm.Table[, 4]
  perm.Sum.Genes.H2O = perm.Table[, 5] + perm.Table[, 6] + perm.Table[, 7] + perm.Table[, 8]
  perm.Matrix = cbind(perm.Sum.Genes, perm.Sum.Genes.H2O)
  perm.All.Reads = sum(perm.Matrix[, 1])
  perm.All.Reads.H2O = sum(perm.Matrix[, 2])
  
  pvalues = apply(perm.Matrix, 1, function(x) fisher.test(matrix(c(
    x[1],
    x[2],
    perm.All.Reads - x[1],
    perm.All.Reads.H2O - x[2]
  ), ncol = 2))$p.value)
  results[i] = sum(pvalues < pvalue.New)
}

FDR = mean(results) / adjusted.Genes.Count # 0.4918
