name = c('Ivan', 'Anna', 'Vasya')
surname = c('Ivanov', 'Petrova', 'Sidorov')
factorSurname = factor(surname)
age = c(23, 23, 21)
gender = c('M', 'F', 'M')

peopleData = data.frame(name, factorSurname, age, gender)
peopleData$name
peopleData[2,]

peopleData[peopleData$factorSurname == 'Ivanov',]
peopleData[peopleData$gender == 'M',]


sourceValues = runif(100, 0, 7)
sinValues = sourceValues[sin(sourceValues) > 0]
matrixSourceValues = matrix(sourceValues, nrow = 10, ncol = 10)
resultValues = matrixSourceValues[matrixSourceValues[,6] > 4,
                                  apply(matrixSourceValues, 2, min) < 1]