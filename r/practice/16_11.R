data.Normal = matrix(rnorm(50000, 0, 1), ncol = 5)
data.Cancer = matrix(rnorm(50000, 0, 1), ncol = 5)

for (i in 1:500) {
  data.Cancer[i, ] = rnorm(5, 3, 1)
}

pvalues = sapply(1:nrow(data.Normal), function (i) t.test(data.Normal[i, ], data.Cancer[i, ])$p.value)
regular.Meaningful.Genes = length(pvalues[pvalues < 0.05]) # 905

indices = which(pvalues < 0.05)
falsePositive = length(indices[indices > 500]) # 415
falsePositive.Perc = falsePositive / regular.Meaningful.Genes # 0.45

new.Pvalue = p.adjust(pvalues, method = "BH")
new.Pvalue.Closest = max(new.Pvalue[new.Pvalue < 0.05])
new.Pvalue.Limit = pvalues[which(new.Pvalue == new.Pvalue.Closest)]

regular.Meaningful.Genes.New = length(pvalues[pvalues < new.Pvalue.Limit]) # 140
indices.New = which(pvalues < new.Pvalue.Limit)
falsePositive.New = length(indices.New[indices.New > 500]) # 3
falsePositive.New.Perc = falsePositive.New / regular.Meaningful.Genes.New # 0.02

B.Pvalue = p.adjust(pvalues, method = "bonferroni")
B.Pvalue.Closest = max(B.Pvalue[B.Pvalue < 0.05])
B.Pvalue.Limit = pvalues[which(B.Pvalue == B.Pvalue.Closest)]

regular.Meaningful.Genes.B = length(pvalues[pvalues < B.Pvalue.Limit]) # 2
indices.B = which(pvalues < B.Pvalue.Limit)
falsePositive.New = length(indices.New[indices.New > 500]) # 0
falsePositive.New.Perc = falsePositive.New / regular.Meaningful.Genes.New # 0