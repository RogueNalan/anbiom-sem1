first = rpois(200, 0.5)
second = rpois(200, 3)

firstDensity = density(first)
secondDensity = density(second)

par(mfrow = c(1, 2))
plot(firstDensity, xlim = c(0, max(max(first), max(second))))
lines(secondDensity, col = 'red')
plot(first, second)
abline(lm(second ~ first), col = 'red')

cor(first, second)
cor.test(first, second, m = 'p')$p.value

both = cbind(first, second)
write.table(both, '1.txt')
