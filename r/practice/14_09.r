nextelement = function(prev, b, index) {
  return (-1 * (prev * b * b) / (index - 1) / (index))
}

taylor = function(a, elements) {
  result = a
  current = a
  for (i in 2:elements) {
    current = nextelement(current, a, (i * 2) - 1)
    result = result + current
  }
  return (result)
}

num = runif(1, 0, 3)
delta = 1e-4;
sin_value = sin(num)

elems = 1:12
results = c()
for (i in elems) {
  results[i] = taylor(num, i)
}

par(mfrow=c(1,2))
plot(elems, abs(results - sin_value), log='y', t='l')

more_numbers = runif(100, -10, 10)
more_numbers = sort(more_numbers)

taylored = taylor(more_numbers, 15)
sin_values = sin(more_numbers)
diffs = abs(sin_values - taylored)

plot(more_numbers, diffs, log = 'y', t = 'l')