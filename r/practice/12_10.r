find.Nth = function(vec, nth) {
  if (((typeof(vec) != "double") && (typeof(vec) != "integer")) ||
       (length(vec) < nth) ||
       (nth < 1))
    return (NA);
  
  vec[order(-vec)][nth]
}
find.Nth(1:5, 2)

norm.Values = function(values, margin, func, norm.Method) {
  norm.Stats = apply(values, margin, func)
  sweep(values, margin, norm.Stats, norm.Method)
}

m = matrix(rnorm(99, 5), ncol=9)
norm1 = norm.Values(m, 1, mean, `/`)
norm2 = norm.Values(m, 2, mean, `-`)

m2 = matrix(runif(99, 5, 10), ncol = 11)
quantileIndex = floor(length(m2) / 4)
m2.Quantile = norm.Values(m2, 1, function(x) find.Nth(m2, quantileIndex), `/`)