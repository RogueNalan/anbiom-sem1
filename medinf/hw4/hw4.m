Data = readtable('Data.xlsx')

Data.dtArrive = datenum(Data.dtArrive)
Data.dtBegin = datenum(Data.dtBegin)

MinutesInDay = 24 * 60
Data.WaitPatient = MinutesInDay * (Data.dtBegin - Data.dtArrive)

% Merge exams for the same patient visit
Data = sortrows(Data, {'stModality', 'stMRN', 'dtBegin'});
Data.RepeatedMRN = zeros(size(Data.stMRN));
nData = length(Data.dtArrive);
for n=2:nData
if Data.stMRN(n-1)==Data.stMRN(n) & Data.dtArrive(n) < Data.dtBegin(n-1)
Data.RepeatedMRN(n) = 1; % same visit record, we can remove it
end;
end;
% Remove all repeats
rows = (Data.RepeatedMRN==0);
Data = Data(rows, :);
Data.RepeatedMRN = [];

% Build wait line sizes
Data = sortrows(Data, {'dtArrive'});

 % initialize wait line sizes
Data.L0 = zeros(size(Data.stMRN));
Data.L1 = Data.L0;
Data.L2 = Data.L0;

nData = length(Data.dtArrive);
for n=1:nData
    tnow = Data.dtArrive(n); % we predict wait time for new arrivals only
    inds = find(Data.dtArrive<tnow & Data.dtBegin>tnow);
    Data.L0(n) = length(inds);
end;

save('Data.mat', 'Data');
I = ones(size(Data.L0));
Y = Data.WaitPatient;

% Computing regression for task 1
[b, bint, r, rint, stats] = regress(Y, [I]);
rmedianI = median(abs(r)); % 5.3119
R5minI = length(r(abs(r) <= 5))/length(r) % 0.4696

% Computing regression for task 2
% Differs from previous example only by presence of the Data.LO predictor
[b, bint, r, rint, stats] = regress(Y, [I Data.L0]);
rmedianIL0 = median(abs(r)); % 3.5296
R5minIL0 = length(r(abs(r) <= 5))/length(r) % 0.6284

% Initialize L1 values
FiveMinutesInDayPart = 5 / MinutesInDay;
for n=1:nData
    tnow = Data.dtArrive(n) - FiveMinutesInDayPart; % we predict wait time for new arrivals only
    inds = find(Data.dtArrive<tnow & Data.dtBegin>tnow);
    Data.L1(n) = length(inds);
end;

% Computing regression for task 3
[b, bint, r, rint, stats] = regress(Y, [I Data.L0 Data.L1]);
rmedianIL0L1 = median(abs(r)); % 3.4529
R5minIL0L1 = length(r(abs(r) <= 5))/length(r) % 0.6352

% Excluding most unpredictable patients
q = quantile(abs(r), 0.8); % 8.5321
inds = find(abs(r) < q);
DataL3 = Data(inds, :);
YL3 = DataL3.WaitPatient;
IL3 = ones(size(DataL3.L0))

% Computing regressaion for task 4
[b, bint, r, rint, stats] = regress(YL3, [IL3 DataL3.L0 DataL3.L1]);
rmedianIL0L1Best = median(abs(r)); % 2.6137
R5minIL0L1Best = length(r(abs(r) <= 5))/length(r) % 0.8018