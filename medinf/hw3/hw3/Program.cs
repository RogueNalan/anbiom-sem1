﻿using Accord.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace hw3
{
    class Program
    {
        static void Main(string[] args)
        {
            var matrix = new MatReader(@"Data.mat");
            double[,] data = (double[,])matrix["Data"].Value;

            var parsedExamData = ParseExamData(data);

            int mostBiased = parsedExamData.AsParallel()
                .GroupBy(ped => ped.TechId)
                .Where(tped => tped.Count() >= 10)
                .Select(tped => new { Key = tped.Key, Percentage = (double)tped.Sum(ed => ed.BiasedTimestamps) / (tped.Count() * 3) })
                .OrderByDescending(pct => pct.Percentage)
                .First()
                .Key;

            int maxLineLength = GetMaxLineLength(parsedExamData);

            Console.WriteLine($"Tech with Id {mostBiased} is the most biased.");
            Console.WriteLine($"Longest line length were {maxLineLength} people.");
            Console.ReadLine();
        }

        private static IEnumerable<ExamData> ParseExamData(double[,] data)
        {
            int length = data.GetLength(0);
            int rowLength = data.GetLength(1);
            int doubleSize = sizeof(double);
            int copyLength = doubleSize * rowLength;

            for (int i = 0; i < length * copyLength; i += copyLength)
            {
                double[] examData = new double[rowLength];
                Buffer.BlockCopy(data, i, examData, 0, copyLength);
                yield return ExamData.Create(examData);
            }
        }

        private static int GetMaxLineLength(IEnumerable<ExamData> parsedExamData)
        {
            var orderedExamData = parsedExamData.OrderBy(ped => ped.PatientArrivalTime).ToList();
            var lockObject = new object();

            int maxLineLength = 1;
            Parallel.For(0, orderedExamData.Count, i =>
            {
                int lineLength = orderedExamData.Take(i + 1).Count(oed => oed.ExamBeginTime >= orderedExamData[i].PatientArrivalTime);

                if (lineLength > maxLineLength)
                {
                    lock (lockObject)
                    {
                        if (lineLength > maxLineLength)
                        {
                            maxLineLength = lineLength;
                        }
                    }
                }
            });

            return maxLineLength;
        }
    }
}
