﻿using System;

namespace hw3
{
    sealed class ExamData
    {
        private readonly Lazy<int> biasedTimestamps;

        public int PatientId { get; }

        public DateTime PatientArrivalTime { get; }

        public DateTime ExamBeginTime { get; }

        public DateTime ExamCompleteTime { get; }

        public int TechId { get; }

        public int BiasedTimestamps { get => this.biasedTimestamps.Value; }

        public static ExamData Create(double[] values)
        {
            if (values == null)
                throw new ArgumentNullException(nameof(values));

            if (values.Length != 5)
                throw new ArgumentException(nameof(values));

            return new ExamData(
                (int)Math.Round(values[0]),
                GetDateTime(values[1]),
                GetDateTime(values[2]),
                GetDateTime(values[3]),
                (int)Math.Round(values[4]));
        }

        private ExamData(int patientId, DateTime arrivalTime, DateTime examBeginTime, DateTime examCompleteTime, int techId)
        {
            this.PatientId = patientId;
            this.PatientArrivalTime = arrivalTime;
            this.ExamBeginTime = examBeginTime;
            this.ExamCompleteTime = examCompleteTime;
            this.TechId = techId;

            this.biasedTimestamps = new Lazy<int>(GetBiasedTimestampsCount);
        }

        private int GetBiasedTimestampsCount()
        {
            int count = 0;

            if (IsBiased(PatientArrivalTime))
                ++count;

            if (IsBiased(ExamBeginTime))
                ++count;

            if (IsBiased(ExamCompleteTime))
                ++count;

            return count;
        }

        private static DateTime GetDateTime(double datenum)
        {
            double ticks = (datenum - 367) * 86400 / 1e-7;
            return new DateTime((long)Math.Round(ticks));
        }

        private static bool IsBiased(in DateTime dateTime) => dateTime.Minute % 10 == 0;
    }
}
