﻿using System;
using System.Collections.Generic;

namespace hw2
{
    sealed class Patient
    {
        public string Name { get; }

        public DateTime DateOfBirth { get; }

        internal Patient(string name, DateTime dateOfBirth)
        {
            this.Name = name;
            this.DateOfBirth = dateOfBirth;
        }
    }

    sealed class PatientNameComparer : IEqualityComparer<Patient>
    {
        public bool Equals(Patient x, Patient y)
        {
            if (x?.Name != null)
            {
                return x.Name.Equals(y.Name, StringComparison.OrdinalIgnoreCase);
            }
            else
            {
                return x?.Name == y?.Name;
            }
        }

        public int GetHashCode(Patient obj) => obj.Name.ToUpperInvariant().GetHashCode();
    }
}
