﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace hw2
{
    static class PatientReader
    {
        private const int separatorNumber = 4;

        private static readonly Regex pidRegex = new Regex(@"\s*PID\s*", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private static readonly Regex regexNameSeparator = new Regex(@"\^+", RegexOptions.Compiled);

        public static Patient Read(string hl7filename)
        {
            if (string.IsNullOrEmpty(hl7filename) || !File.Exists(hl7filename))
            {
                throw new ArgumentException(nameof(hl7filename));
            }

            string hl7Data = File.ReadAllText(hl7filename);
            var separators = GetSeparators(hl7Data);
            string[] splittedHl7Data = hl7Data.Split(separators[0]);

            return GetPatient(splittedHl7Data, separators[1]);
        }

        private static char[] GetSeparators(string hl7Data)
        {
            int index = GetSeparatorArrayIndex(hl7Data);

            var separators = new char[separatorNumber];
            for (int i = 0; i < separatorNumber; ++i)
            {
                char c = hl7Data[index + i];
                if (Char.IsLetterOrDigit(c))
                {
                    throw new ArgumentException(nameof(hl7Data));
                }

                separators[i] = c;
            }

            return separators;
        }

        private static Patient GetPatient(string[] splittedHl7Data, char nameSeparator)
        {
            const int nameOffset = 5;
            const int dateOfBirthOffset = 7;

            int pidIndex = Array.FindIndex(splittedHl7Data, field => pidRegex.IsMatch(field));
            if (pidIndex == -1)
            {
                throw new ArgumentException(nameof(splittedHl7Data));
            }

            string name = GetName(splittedHl7Data[pidIndex + nameOffset], nameSeparator);
            DateTime dateOfBirth = GetDateOfBirth(splittedHl7Data[pidIndex + dateOfBirthOffset]);

            return new Patient(name, dateOfBirth);
        }

        private static int GetSeparatorArrayIndex(string hl7Data)
        {
            int index = 0;
            while (index < hl7Data.Length && Char.IsLetterOrDigit(hl7Data[index]))
            {
                ++index;
            }

            if (index + separatorNumber > hl7Data.Length)
            {
                throw new ArgumentException(nameof(hl7Data));
            }

            return index;
        }

        private static string GetName(string nameField, char nameSeparator)
        {
            const char commonNameSeparator = '^';

            string replacedCharacter = nameField.Replace(nameSeparator, commonNameSeparator);
            return regexNameSeparator.Replace(replacedCharacter, " ").Trim();
        }

        private static DateTime GetDateOfBirth(string dateOfBirthField) =>
            DateTime.ParseExact(dateOfBirthField, "yyyyMMdd", CultureInfo.InvariantCulture);
    }
}
