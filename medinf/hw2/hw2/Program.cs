﻿using System;
using System.IO;
using System.Linq;

namespace hw2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input directory:");
            string directoryName = Console.ReadLine();

            if (!Directory.Exists(directoryName))
            {
                throw new ArgumentException(nameof(directoryName));
            }

            var files = Directory.GetFiles(directoryName);
            var patients = files.AsParallel().Select(PatientReader.Read);
            int minYear = patients.Min(patient => patient.DateOfBirth.Year);
            int uniquePatients = patients.Distinct(new PatientNameComparer()).Count();

            Console.WriteLine($"Oldest patient year of birth: {minYear}, unique patient names: {uniquePatients}");

            Console.ReadLine();
        }
    }
}
