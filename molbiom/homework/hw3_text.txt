COGs — Clusters of Orthologous Groups (COGs)
NOGD — The non-orthologous gene displacement

1. COMPARATIVE GENOMICS,MINIMAL GENE-SETS AND THE LAST UNIVERSAL COMMON ANCESTOR
2. Phylogenetic tree
3. Having a list of essential genes might eventually enable a biological
engineer to manipulate a cellular system to perform desirable functions.
A good model for a minimal genome is Mycoplasma genitalium due to its very small genome size. Most genes that are used by this organism are usually considered essential for survival; based on this concept a minimal set of 256 genes has been proposed
4. COG (list of genes more similar to each other than any other genes) + NOGD
5. Minimal Gene-Set is environment-dependant, essentials. M. genitalium, with ~480 genes, is a good
case in point, and this genome was dubbed ‘minimal’ at the time of its sequencing. No proof, virus could've interacted
6. When two genomes are examined, it is apparent that orthologues are often duplicated after the speciation event that led to the divergence of the
species that are being compared. This results in more complex situations, in which two or more genes in one genome are co-orthologous to one gene in the
other genome.
7. NOGDs are found by analyzing which proteins help with missing cell functions. NOGD occur in the most essential genes
8. Experimental approach — removing genes from bacteria. This early experimental analysis, although it was done on a limited scale, indicated that the minimal gene-set derived from the
B. subtilis genome, which contains a total of ~4,100 genes, might consist of ~300 genes. This number is remarkably close to 256, the minimal set derived by
computational genomics based on the M. genitalium/H. influenzae comparison.
9. The crucial first step, which is technically feasible, if challenging, is the identification of complete sets
of essential genes. Genetic methods used for this type of analysis include transposon-insertion mutagenesis,
plasmid-insertion mutagenesis29 and the inactivation of genes using antisense RNAs30. Genome-wide analyses of gene knockouts produced using these approaches
have been reported for several bacteria and two eukaryotes (TABLE 3). Although, for technical reasons, none of these studies succeeded in mutagenizing all the genes
in the respective genomes, more than 50% of genes have been disrupted in each case, which is sufficient for reliable extrapolations.
10. N/A (picture)
11. Computational: some NOGDs remain undetected
Experimental: false positive essential genes (removing which slows down the organism growth, but doesn’t kill)
12. LUCA: one or many. It has been suggested that HGT might have been so rampant that the feasibility and meaningfulness of a species tree would become dubious, although
an argument was also made that the extent of HGT might have been overestimated, mainly due to artefacts of phylogenetic methods.
Assuming that a species tree makes sense, phyletic patterns of orthologous-gene clusters can be mapped onto the branches of the tree. By using one of the
implementations of the EVOLUTIONARY PARSIMONY principle, we can reconstruct the evolutionary scenario that includes the smallest number of elementary events (the
most parsimonious scenario). The elementary events in the evolution of genes are the emergence of a gene, gene loss and HGT.
However, this is the most parsimonious scenario only if we assume that gene loss and HGT are equally likely. By contrast, if HGT were much less frequent than
gene loss, the most parsimonious scenario for COG1646 in FIG. 4b would consist of seven losses, and the COG would have been inferred to descend from
LUCA. The main problem with these reconstructions is that we have no reliable estimate of the actual relative rates (probabilities) of gene loss and HGT. It is often
hypothesized that gene loss is (much) more common because it is mechanistically easier and because it has occurred en masse in many parasites. However,there is
no hard evidence to support this notion, and it is suspected that a lot of HGT might go unnoticed.
13. N/A (picture)
14. A definitive solution is probably out of our reach, but a crude one can be obtained by reverting to
the minimal gene-set approach or, more precisely, examination of the state of essential functional niches in
the reconstructed gene-set of LUCA. It is possible to trace how these niches are filled with the increasing size
of the reconstructed LUCA or, to use technical jargon, with an increasing g value. This type of analysis
becomes particularly convincing when, as well as individual functions, the completion of entire metabolic
pathways is examined, as illustrated in FIG. 6. A systematic survey showed that most, if not all, known
essential pathways are filled up with genes at g = 1 — when one assumes that gene loss and HGT are equally common, and ~600 genes are assigned to LUCA42.
