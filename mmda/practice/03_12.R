library(caret)
library(glmnet)

load("training.RData")
load("testing.RData")

str(training)

fullSet = names(training)[names(training) != "Class"]
predCorr = cor(training[, fullSet])
highCorr = findCorrelation(predCorr, .99)
fullSet = fullSet[-highCorr]

isNZV = nearZeroVar(training[, fullSet], saveMetrics = TRUE, freqCut = floor(nrow(training)) / 5)
reducedSet = rownames(subset(isNZV, !nzv & freqRatio < floor(nrow(training) / 50)))
reducedSet = reducedSet[(reducedSet != "allPub") &
                           (reducedSet != "numPeople") &
                           (reducedSet != "Mar") &
                           (reducedSet != "Sun")
                         ]

ctrl = trainControl(summaryFunction = twoClassSummary, classProbs = TRUE, savePredictions = TRUE)
set.seed(476)
lrReduced = train(training[,reducedSet], y = training$Class, method = "glm", metric = "ROC", trControl = ctrl)
confusionMatrix(data = lrReduced$pred$pred, reference = lrReduced$pred$obs)

library(MASS)
set.seed(476)
ldaFit <- train(x = training[, reducedSet],
                y = training$Class,
                method = "lda",
                preProc = c("center","scale"),
                metric = "ROC",
                trControl = ctrl)

ldaTestClasses <- predict(ldaFit, newdata = testing[,reducedSet])
ldaTestProbs <- predict(ldaFit, newdata = testing[,reducedSet], type = "prob")
plot(testing$Class, ldaTestClasses)

set.seed(476)
plsFit <- train(x = training[, reducedSet],
                y = training$Class,
                method = "pls",
                tuneGrid = expand.grid(.ncomp = 1:10),
                preProc = c("center","scale"),
                metric = "ROC",
                trControl = ctrl)


library(pROC)
reducedRoc = roc(response = lrReduced$pred$obs, 
                  predictor = lrReduced$pred$successful, 
                  levels = rev(levels(lrReduced$pred$obs))) 
plot(reducedRoc, legacy.axes = TRUE) 



lrReducedClasses <- predict(lrReduced, newdata = testing[,reducedSet])
lrReducedClassesProb <- predict(lrReduced, newdata = testing[,reducedSet], type="prob")
confusionMatrix(data = lrReducedClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = lrReducedClassesProb[,1], levels = rev(levels(testing$Class))) 
plot(rocCurve)
confusionMatrix(data = lrReduced$pred$pred, reference = lrReduced$pred$obs)

lrTestClasses <- predict(lrReducedClasses, newdata = testing[,reducedSet])
ldaTestProbs <- predict(lrReducedClasses, newdata = testing[,reducedSet], type = "prob")
plot(testing$Class, ldaTestClasses)

confusionMatrix(data = lrTestClasses, reference = testing$Class, positive = "successful")
reducedRoc = roc(response = testing$Class, predictor = lrTestProbs$successful, levels = rev(levels(testing$Class)))

confusionMatrix(data = ldaTestClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = ldaTestProbs[,1], levels = rev(levels(testing$Class))) 
plot(rocCurve)

set.seed(476)
plsFit <- train(x = training[, reducedSet],
                y = training$Class,
                method = "pls",
                tuneGrid = expand.grid(.ncomp = 1:10),
                preProc = c("center","scale"),
                metric = "ROC",
                trControl = ctrl)
plsFitClasses = predict(plsFit, newdata = testing[,reducedSet])
plsFitClassesProb = predict(plsFit, newdata = testing[,reducedSet], type = "prob")
confusionMatrix(data = plsFitClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = plsFitClassesProb[,1], levels = rev(levels(testing$Class))) 
plot(rocCurve)

plsImpGrant = varImp(plsFit, scale = FALSE)
