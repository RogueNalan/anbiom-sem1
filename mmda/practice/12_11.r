library(AppliedPredictiveModeling)
library(caret)
data("solubility")

install.packages("rpart")
library(rpart)

trainData = solTrainXtrans
trainData$y = solTrainY

split1 = rpart(y ~ ., data = trainData,
               control = rpart.control(maxdepth = 1))

split2 = rpart(y ~ ., data = trainData,
                control = rpart.control(maxdepth = 2))

split3 = rpart(y ~ ., data = trainData,
                control = rpart.control(maxdepth = 3))

split20 = rpart(y ~ ., data = trainData,
                control = rpart.control(maxdepth = 20))
set.seed(100)
indx = createFolds(solTrainY, returnTrain = TRUE)
ctrl = trainControl(method = "cv", index = indx)

cartTune = train(x = solTrainXtrans, y = solTrainY,
                 method = "rpart",
                 tuneLength = 25,
                 trControl = ctrl)

install.packages("partykit")
library(partykit)

cartTree = as.party(cartTune$finalModel)
plot(cartTree)

cartImp = varImp(cartTune, scale = FALSE, competes = FALSE)

testResults = data.frame(obs = solTestY, pred = predict(cartTune, solTestXtrans))
defaultSummary(testResults)

# RMSE = 0.8654
# R2 = 0.8269

set.seed(100)
treebagTune = train(x = solTrainXtrans,
                    y = solTrainY,
                    method = "treebag",
                    nbagg = 50,
                    trControl = ctrl)

observed = solTestY
predicted = predict(treebagTune, solTestXtrans)
defaultSummary(data.frame(obs = observed, pred = predicted))

install.packages("randomForest")
library(randomForest)

mtryGrid = data.frame(mtry = floor(seq(10, ncol(solTrainXtrans), length = 10)))
set.seed(100)
rfTune = train(x = solTrainXtrans, y = solTrainY,
               method = "rf",
               tuneGrid = mtryGrid,
               ntree = 100,
               importance = TRUE,
               trControl = ctrl)
predicted = predict(rfTune, solTestXtrans)
defaultSummary(data.frame(obs = observed, pred = predicted))

rfImp = varImp(rfTune, scale = FALSE)

install.packages("gbm")
library(gbm)

gbmGrid = expand.grid(interaction.depth = 7,
                      n.trees = 200,
                      shrinkage = 0.1,
                      n.minobsinnode = 1)
set.seed(100)
gbmTune = train(x = solTrainXtrans, y = solTrainY,
                method = "gbm",
                tuneGrid = gbmGrid,
                trControl = ctrl,
                verbose = FALSE)

predicted = predict(gbmTune, solTestXtrans)
defaultSummary(data.frame(obs = observed, pred = predicted))
