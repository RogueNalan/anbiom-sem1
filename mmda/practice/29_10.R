install.packages("MASS")

library(caret)
library(MASS)
library(AppliedPredictiveModeling)
data("solubility")

ls(pattern = "^solT")

trainingData = solTrainXtrans
trainingData$Solubility = solTrainY

lmFitAllPredictors = lm(Solubility ~ ., data = trainingData)
summary(lmFitAllPredictors)

lmPred1 = predict(lmFitAllPredictors, solTestXtrans)
head(lmPred1)

lmValues1 <- data.frame(obs = solTestY, pred = lmPred1)
defaultSummary(lmValues1)

observed = as.vector(solTestY)
predicted = as.vector(lmPred1)

residualValues = observed - predicted
summary(residualValues)

axisRange = extendrange(c(observed, predicted))
plot(observed, predicted, ylim = axisRange, xlim = axisRange)
abline(0, 1, col = "darkgrey", lty = 2)
plot(predicted, residualValues, ylab = "residual") > abline(h = 0, col = "darkgrey", lty = 2)

RMSE(predicted, observed)
R2(predicted, observed)

cor(predicted, observed)
cor(predicted, observed, method = "spearman")

rlmFitAllPredictors = rlm(Solubility ~ ., data = trainingData)
summary(rlmFitAllPredictors)

lmPred2 = predict(rlmFitAllPredictors, solTestXtrans)
head(lmPred2)

lmValues2 <- data.frame(obs = solTestY, pred = lmPred2)
defaultSummary(lmValues2)

observed = as.vector(solTestY)
predicted = as.vector(lmPred2)

residualValues2 = observed - predicted
summary(residualValues2)

axisRange = extendrange(c(observed, predicted))
plot(observed, predicted, ylim = axisRange, xlim = axisRange)
abline(0, 1, col = "darkgrey", lty = 2)
plot(predicted, residualValues, ylab = "residual") > abline(h = 0, col = "darkgrey", lty = 2)

RMSE(predicted, observed)
R2(predicted, observed)

cor(predicted, observed)
cor(predicted, observed, method = "spearman")

set.seed(100)
ctrl = trainControl(method = "cv", number = 10)
lmFit1 = train(x = solTrainXtrans, y = solTrainY, method = "lm", trControl = ctrl)
lmFit1$results

xyplot(solTestY ~ predict(lmFit1), type = c("p", "g"), xlab = "Predicted", ylab = "Observed")
x11()
xyplot(resid(lmFit1) ~ predict(lmFit1), type = c("p", "g"), xlab = "Predicted", ylab = "Residuals")

colThresh = .9
tooHigh = findCorrelation(cor(solTrainXtrans), colThresh)
corrPred = names(solTrainXtrans)[tooHigh]
trainXfiltered = solTrainXtrans[, -tooHigh]
testXfiltered = solTestXtrans[, -tooHigh]

set.seed(100)
lmFiltered = train(trainXfiltered, solTrainY, method = "lm", trControl = ctrl)
lmFiltered

set.seed(100)
rlmPCA = train(solTrainXtrans, solTrainY, method = 'rlm', preProcess = 'pca', trControl = ctrl)
rlmPCAPredicted = predict(rlmPCA, solTestXtrans)
predicted = as.vector(rlmPCAPredicted)
observed = solTestY
RMSE(predicted, observed)
R2(predicted, observed)

install.packages("pls")
library(pls)
library(caret)

plsFit = plsr(Solubility ~ ., data = trainingData)
plsFitPredicted = predict(plsFit, solTestXtrans, ncomp = 1)
predicted = as.vector(plsFitPredicted)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)

plsFit = plsr(Solubility ~ ., data = trainingData)
plsFitPredicted = predict(plsFit, solTestXtrans, ncomp = 10)
predicted = as.vector(plsFitPredicted)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)

plsFit = plsr(Solubility ~ ., data = trainingData)
plsFitPredicted = predict(plsFit, solTestXtrans, ncomp = 219)
predicted = as.vector(plsFitPredicted)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)

indx = createFolds(solTrainY, returnTrain = TRUE)
set.seed(100)
plsTune = train(x = solTrainXtrans, y = solTrainY,
                method = "pls",
                tuneGrid = expand.grid(ncomp = 1:20),
                trControl = ctrl)
plsTune
plot(plsTune)
plsResults = predict(plsTune, solTestXtrans)
observed = as.vector(solTestY)
predicted = as.vector(plsResults)
RMSE(predicted, observed)
R2(predicted, observed)

set.seed(100)
pcrTune = train(x = solTrainXtrans, y = solTrainY,
                method = "pcr",
                tuneGrid = expand.grid(ncomp = 1:35),
                trControl = ctrl)
pcrTune
plot(pcrTune)
pcrResults = predict(pcrTune, solTestXtrans)
predicted = as.vector(pcrResults)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)


plsResamples = plsTune$results
plsResamples$Model = "PLS"
pcrResamples = pcrTune$results
pcrResamples$Model = "PCR"
plsPlotData = rbind(plsResamples, pcrResamples)

xyplot(RMSE ~ ncomp,
       data = plsPlotData,
       groups = Model,
       auto.key = list(columns = 2),
       xlab = "# Components",
       ylab = "RMSE (Cross-Validation)",
       type = c("o", "g"))

x11()
xyplot(Rsquared ~ ncomp,
       data = plsPlotData,
       groups = Model,
       auto.key = list(columns = 2),
       xlab = "# Components",
       ylab = "RMSE (Cross-Validation)",
       type = c("o", "g"))


plsImp = varImp(plsTune, scale = FALSE)
plot(plsImp, top = 25, scales = list(y = list(cex = .95)))

set.seed(100)
ridgeGrid = expand.grid(lambda = seq(0, .1, length = 15))
set.seed(100)
ridgeTune = train(x = solTrainXtrans, y = solTrainY,
                   method = "ridge",
                   tuneGrid = ridgeGrid,
                   trControl = ctrl,
                   preProc = c("center", "scale"))

ridgeResults = predict(ridgeTune, solTestXtrans)
predicted = as.vector(ridgeResults)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)

set.seed(100)
enetGrid = expand.grid(lambda = c(0, 0.01, .1),
                        fraction = seq(.05, 1, length = 20))
enetTune = train(x = solTrainXtrans, y = solTrainY,
                  method = "enet",
                  tuneGrid = enetGrid,
                  trControl = ctrl,
                  preProc = c("center", "scale"))

enetResults = predict(enetTune, solTestXtrans)
predicted = as.vector(enetResults)
observed = as.vector(solTestY)
RMSE(predicted, observed)
R2(predicted, observed)

install.packages("earth")
library(earth)

marsFit = earth(solTrainXtrans, solTrainY)
marsFit

marsGrid = expand.grid(.degree = 1:2, nprune = 2:38)
set.seed(100)
marsTuned = train(solTrainXtrans, solTrainY, method = "earth")
marsTuned

varImp(marsTuned)

marsPredicted = predict(marsTuned, solTestXtrans)
testResults = data.frame(obs = solTestY, mars = marsPredicted)
xyplot(solTestY ~ marsPredicted)

RMSE(pred = marsPredicted, obs = solTestY)
R2(pred = marsPredicted, obs = solTestY, formula = "corr")

library(AppliedPredictiveModeling)
data("solubility")

install.packages("ksvm")
library(kernlab)
svmFit = ksvm(x = solTrainXtrans, y = solTrainY, kernel = "rbfdot", kpar = "automatic", C = 1, epsilon = 0.1)
svmRTuned = train(solTrainXtrans, solTrainY, method = "svmRadial")
preProc = c("center", "scale", tuneLength = 14)
svmRTuned
svmRTuned$finalModel

svmPredicted = predict(svmRTuned, solTestXtrans)
testResults = data.frame(obs = solTestY, svm = svmPredicted)
xyplot(solTestY ~ svmPredicted, data = testResults, pch = 19)
RMSE(pred = svmPredicted, obs = solTestY)
R2(pred = svmPredicted, obs = solTestY, formula = "corr")

knnDescr = solTrainXtrans[, -nearZeroVar(solTrainXtrans)]
set.seed(100)
knnTune = train(knnDescr, solTrainY,
                method = "knn",
                preProc = c("center", "scale"),
                tuneGrid = data.frame(.k = 1:20),
                trControl = trainControl(method = "cv"))
knnTune

knnTestX <- solTestXtrans[, -nearZeroVar(solTrainXtrans)]
knnPredicted<-predict(knnTune, knnTestX)
testResults <- data.frame(obs=solTestY,knn=knnPredicted)
xyplot(solTestY~knnPredicted, data=testResults, pch=19)

RMSE(pred = knnPredicted, obs = solTestY)
R2(pred = knnPredicted, obs = solTestY, formula = "corr")

install.packages("nnet")
library(nnet)

tooHigh = findCorrelation(cor(solTrainXtrans), cutoff = .75)
trainXnnet = solTrainXtrans[, -tooHigh]
testXnnet = solTestXtrans[, -tooHigh]
predictors = trainXnnet
outcome = solTrainY
nnetFit = nnet(predictors, outcome,
               size = 5,
               decay = 0.01,
               linout = TRUE,
               trace = FALSE,
               maxit = 500, 
               MaxNWTs = 5 * (ncol(predictors) + 1) + 5 +1)
predicted = predict(nnetFit, testXnnet)
observed = solTestY

RMSE(predicted, observed)
frame = data.frame(obs = observed, pred = predicted)
defaultSummary(frame)

set.seed(100)
indx = createFolds(solTrainY, returnTrain = TRUE)
ctrl = trainControl(method = "cv", index = indx)

nnetGrid = expand.grid(decay = c(0.01), size = c(3), bag = FALSE)
set.seed(100)
nnetTune = train(x = trainXnnet, y = solTrainY,
                 method = "avNNet",
                 tuneGrid = nnetGrid,
                 trControl = ctrl,
                 preProc = c("center", "scale"),
                 linout = TRUE,
                 trace = TRUE,
                 MaxNWts = 13 * (ncol(trainXnnet) + 1) + 13 + 1,
                 maxit = 500,
                 allowParallel = TRUE)

plot(nnetTune)
predicted = predict(nnetTune, testXnnet)
observed = solTestY
RMSE(predicted, observed)
frame = data.frame(obs = observed, pred = predicted)
defaultSummary(frame)
