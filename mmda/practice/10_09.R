install.packages("caret")
install.packages("corrplot")
install.packages("e1071")
install.packages("lattice")

library(AppliedPredictiveModeling)
data(segmentationOriginal)

structure(segmentationOriginal)
summary(segmentationOriginal)

segData <- subset(segmentationOriginal, Case == "Train")

Cell <- segData$Cell
Class <- segData$Class
Case <- segData$Case

segData <- segData[, -(1:3)]
statusColNum <- grep("Status", names(segData)) # names() return all columns names in the table
segData <- segData[, -statusColNum]

library(e1071)
skewness(segData$AngleCh1)

skewValues <- apply(segData, 2, skewness)
head(skewValues)

library(caret)
Ch1AreaTrans <- BoxCoxTrans(segData$AreaCh1)
Ch1AreaTrans

plot(segData$AreaCh1)
hist(segData$AreaCh1)

# switched to = from <- as assignment symbol, since it's more usual to me

transformedData = predict(Ch1AreaTrans, segData$AreaCh1)

par(mfrow = c(1, 2))
hist(segData$AreaCh1)
hist(transformedData)

pcaObject = prcomp(segData, center=TRUE, scale=TRUE)
percentVariance = pcaObject$sdev^2/sum(pcaObject$sdev^2)*100
percentVariance
percentVariance[1:3]
head(pcaObject$x[, 1:5])
head(pcaObject$rotation[, 1:3])

# read about data preprocessing in Applied Predictive Modeling book
# read about PCA somewhere