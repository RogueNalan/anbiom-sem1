install.packages('mda')

library(caret)
library(mda)
library(pROC)
library(kernlab)

load("training.RData")
load("testing.RData")
load("reducedSet.RData")
load("fullSet.RData")

colnames(training)
summary(training)
structure(training)

ctrl <- trainControl(summaryFunction = twoClassSummary,
                     classProbs = T,
                     savePredictions = T)


load("mdaFit.RData")
plot(mdaFit)
# mdaFit
ReducedClasses <- predict(mdaFit, newdata = testing[,reducedSet])
ReducedClassesProb <- predict(mdaFit, newdata = testing[,reducedSet], type="prob")
confusionMatrix(data = ReducedClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = ReducedClassesProb[,1], levels = rev(levels(testing$Class)))
plot(rocCurve)
auc(rocCurve)

load("nnetFit.RData")
ReducedClasses <- predict(nnetFit, newdata = testing[,reducedSet])
ReducedClassesProb <- predict(nnetFit, newdata = testing[,reducedSet], type="prob")
confusionMatrix(data = ReducedClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = ReducedClassesProb[,1], levels = rev(levels(testing$Class)))
plot(rocCurve)
auc(rocCurve)

load("svmRModel.RData")
ReducedClasses <- predict(svmRModel, newdata = testing[,reducedSet])
ReducedClassesProb <- predict(svmRModel, newdata = testing[,reducedSet], type="prob")
confusionMatrix(data = ReducedClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = ReducedClassesProb[,1], levels = rev(levels(testing$Class)))
plot(rocCurve)
auc(rocCurve)

load("knnFit.RData")
ReducedClasses <- predict(knnFit, newdata = testing[,reducedSet])
ReducedClassesProb <- predict(knnFit, newdata = testing[,reducedSet], type="prob")
confusionMatrix(data = ReducedClasses, reference = testing$Class, positive = "successful")
rocCurve <- roc(response = testing$Class, predictor = ReducedClassesProb[,1], levels = rev(levels(testing$Class)))
plot(rocCurve)
auc(rocCurve)

load("nBayesFit.RData")
load(file = "nbPredictors.RData")
load(file = "nbTesting.RData")
load(file = "nbTraining.RData")

ReducedClasses <- predict(nBayesFit, newdata = nbTesting[,nbPredictors])
ReducedClassesProb <- predict(nBayesFit, newdata = nbTesting[,nbPredictors], type="prob")
confusionMatrix(data = ReducedClasses, reference = nbTesting$Class, positive = "successful")
rocCurve <- roc(response = nbTesting$Class, predictor = ReducedClassesProb[,1], levels = rev(levels(testing$Class)))
plot(rocCurve)
auc(rocCurve)