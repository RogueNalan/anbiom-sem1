library(AppliedPredictiveModeling)
library(caret)

data(cars)
View(cars)

type = c("convertible", "coupe", "hatchback", "sedan", "wagon")

cars$Type = factor(apply(cars[, 14:18], 1, function(x) type[which(x == 1)]))
cars$Type

carSubset = cars[, c(1, 2, 19)]
head(carSubset)
levels(carSubset$Type)

simpleMod = dummyVars(~Mileage + Type, data = carSubset, levelsOnly = TRUE)
newTable = predict(simpleMod, carSubset)
withInteraction = dummyVars(~Mileage + Type + Mileage:Type, data = carSubset, levelsOnly = TRUE)
withInteraction
newTable1 = predict(withInteraction, head(carSubset))

x = 1:12
sample(x)
sample(x, replace = TRUE)

data(twoClassData)
str(predictors)
str(classes)

set.seed(1)
trainingRows = createDataPartition(classes, p = .80, list = FALSE)
head(trainingRows)

trainPredictors = predictors[trainingRows, ]
trainClasses = classes[trainingRows]

testPredictors = predictors[-trainingRows, ]
testClasses = classes[-trainingRows]

str(trainPredictors)
str(testPredictors)

myTable = predictors
myTable$Class = classes

xyplot(PredictorA ~ PredictorB,
       data = myTable,
       groups = myTable$Class,
       xlab = "predictor 1",
       ylab = "predictor 2",
       auto.key = list(columns = 2),
       type = c("p", "g"),
       main = "Original Data",
       aspect = 1)

testSet = maxDissim(myTable[1,], myTable, 20)

set.seed(1)
repeatedSplits = createDataPartition(trainClasses, p = .80, times = 3)
str(repeatedSplits)

set.seed(1)
resampledPredictorA = createResample(predictors$PredictorA, times = 3)
resampledPredictorB = createResample(predictors$PredictorB, times = 3)

set.seed(1)

trainPredictors = as.matrix(trainPredictors)
knnFit = knn3(x = trainPredictors, y = trainClasses, k = 5)
testPrediction = predict(knnFit, newdata = testPredictors, type = "class")
head(testPrediction)

result = cbind(testPredictors, testClasses, testPrediction)
correctPredictions = length(which(result$testClasses == result$testPrediction))

library(caret)
data("GermanCredit")
fix(GermanCredit)

library("e1071")

GermanCredit <- GermanCredit[, -nearZeroVar(GermanCredit)]
GermanCredit$CheckingAccountStatus.lt.0 <- NULL
GermanCredit$SavingsAccountBonds.lt.100 <- NULL
GermanCredit$EmploymentDuration.lt.1 <- NULL
GermanCredit$EmploymentDuration.Unemployed <- NULL
GermanCredit$Personal.Male.Married.Widowed <- NULL
GermanCredit$Property.Unknown <- NULL
GermanCredit$Housing.ForFree <- NULL

set.seed(100)

inTrain = createDataPartition(GermanCredit$Class, p = .8)[[1]]
GermanCreditTrain = GermanCredit[inTrain, ]
GermanCreditTest = GermanCredit[-inTrain, ]

set.seed(1056)
svmFit = train(Class ~ ., data = GermanCreditTrain, method = "svmRadial")

svmFit = train(Class ~.,
               data = GermanCreditTrain,
               method = "svmRadial",
               preProcess = c("center", "scale"),
               tuneLength = 10,
               trControl = trainControl(method = "repeatedcv"))

predictedClasses = predict(svmFit, GermanCreditTest)
str(predictedClasses)

svmFit = train(Class ~.,
               data = GermanCreditTrain,
               method = "svmRadial",
               preProcess = c("center", "scale"),
               tuneLength = 10,
               trControl = trainControl(method = "repeatedcv", repeats = 5))
plot(svmFit, scales = list(x = list(log = 2)))

predictedClasses <- predict(svmFit, GermanCreditTest)
str(predictedClasses) 

svmFit <- train(Class ~ .,
                  data = GermanCreditTrain,
                  method = "svmRadial",
                  preProc = c("center", "scale"),
                  tuneLength = 10,
                  trControl = trainControl(method = "repeatedcv", 
                                           repeats = 5,
                                           classProbs = TRUE))

preictedProbs = predict(svmFit, newdata = GermanCreditTest, type = "prob")
head(preictedProbs)
