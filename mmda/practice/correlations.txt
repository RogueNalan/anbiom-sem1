RMSE: 0.7455802
R2:  0.8722236
Cor1: 0.9339291
Cor2: 0.9207759

RMSE: 0.7529670
R2: 0.8700394
Cor1: 0.932759
Cor2: 0.9192863

RMSE: 0.7324162
R2: 0.8734343

RMSE: 0.7119
R2: 0.8813

rlmPCA:
RMSE: 0.7869
R2: 0.8572

PlsFit, n = 1:
RMSE: 1.7427
R2: 0.29

PlsFit, n = 200
RMSE = 0.7618
R2 = 0.8665

PlsFit, n = 219
RMSE = 0.760
R2 = 0.8669

PlsTune, n = 12
RMSE = 0.7146
R2 = 0.8819

PcrTune, n = 35
RMSE = 0.831
R2 = 0.8508

ridge
RMSE: 0.7199

enet:
RMSE: 0.7072

mars:
RMSE: 0.7318

svm:
RMSE: 0.6080

knn:
RMSE: 1.1066

nnet:
RMSE: 1.4237
R2: 0.6324

nnetTune:
RMSE: 0.8296
R2: 0.8411

CARD:
RMSE: 0.8654
R2: 0.8269

treebagTune:
RMSE: 0.8538
R2: 0.8344

rfTune:
RMSE: 0.64
R2: 0.9055

gbmTune:
RMSE: 0.6062
R2: 0.9149

LDA:
Accuracy = 0.816
Sensitivity = 0.804
Specificity = 0.823

PLS:
Accuracy = 0.841
Sensitivity = 0.854

mdaFit:
Accuracy = 0.8494
Sensitivity = 0.8254

nnetFit:
Accuracy = 0.8417
Sensitivity = 0.8201

svmModel:
Accuracy = 0.8398
Sensitivity = 0.8201

knn:
Accuracy = 0.6969
Sensitivity = 0.3069

nBayes:
Accuracy = 0.668
Sensitivity = 0.5873