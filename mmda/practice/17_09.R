library(AppliedPredictiveModeling)
data(segmentationOriginal)

structure(segmentationOriginal)
summary(segmentationOriginal)

segData <- subset(segmentationOriginal, Case == "Train")

Cell <- segData$Cell
Class <- segData$Class
Case <- segData$Case

trans <- preProcess(segData, method = c("BoxCox", "center", "scale", "pca"))
transformed <- predict(trans, segData)

segTrain <- subset(segmentationOriginal, Case == "Train")
segTrainX <- segTrain[, -(1:3)]
segTrainClass <- segTrain$Class

segPP <- preProcess(segTrain, method = "BoxCox")
segTrainTrans = predict(segPP, segTrain)

par(mfrow = c(1, 2))

xyplot(AvgIntenCh1 ~ EntropyIntenCh1,
       data = segTrainTrans,
       groups = segTrain$Class,
       xlab = "Channel 1 Fiber Width",
       ylab = "Intensity Entropy Channel 1",
       auto.key = list(columns = 2),
       type = c("p", "g"),
       main = "Original Data",
       aspect = 1)

pr <- prcomp(~ AvgIntenCh1 + EntropyIntenCh1,
             data = segTrainTrans,
             scale. = TRUE)

xyplot(PC2 ~ PC1,
       data = as.data.frame(pr$x),
       groups = segTrain$Class,
       xlab = "Principal Component #1",
       ylab = "Principal Component #2",
       main = "Transformed",
       xlim = extendrange(pr$x),
       ylim = extendrange(pr$x),
       type = c("p", "g"),
       aspect = 1)

x11()

xyplot(PC2 ~ PC1,
       data = transformed,
       groups = segTrain$Class,
       xlab = "Principal Component #1",
       ylab = "Principal Component #2",
       main = "Transformed",
       type = c("p", "g"),
       aspect = 1)

isZV <- apply(segTrainX, 2, function(x) length(unique(x)) == 1)
segTrainX <- segTrainX[, !isZV]

segPP <- preProcess(segTrainX, c("BoxCox", "center", "scale"))
segTrainTrans <- predict(segPP, segTrainX)

segPCA <- prcomp(segTrainTrans, center = TRUE, scale. = TRUE)

transparentTheme(pchSize = .8, trans = .3)

panelRange <- extendrange(segPCA$x[, 1:3])
splom(as.data.frame(segPCA$x[, 1:3]),
      groups = segTrainClass,
      type = c("p", "g"),
      as.table = TRUE,
      auto.key = list(columns = 2),
      prepanel.limits = function (x) panelRange)

nearZeroVar(segData)
# 0, no zero-variance

nearZeroVar(segmentationOriginal)

correlations <- cor(segData)
dim(correlations)

install.packages("corrplot")
library(corrplot)

x11()
corrplot(correlations, order = "hclust")

highCorr <- findCorrelation(correlations, cutoff = .75)
length(highCorr)

head(highCorr)
filteredSegData <- segData[, -highCorr]
