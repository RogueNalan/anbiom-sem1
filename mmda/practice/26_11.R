install.packages(c("AppliedPredictiveModeling", "caret", "glmnet", "MASS", "pamr", "pls", "pROC", "rms",
                 "sparseLDA"))
library(caret)
library(glmnet)

load("training.RData")
load("testing.RData")

str(training)
str(testing)

load("pre2008Data.RData")
load("year2008Data.RData")

str(pre2008Data)
str(year2008Data)

fullSet = names(training)[names(training) != "Class"]

predCorr = cor(training[, fullSet])
highCorr = findCorrelation(predCorr, .99)
fullSet = fullSet[-highCorr]

isNZV = nearZeroVar(training[, fullSet], saveMetrics = TRUE, freqCut = floor(nrow(training)) / 5)
fullSet = rownames(subset(isNZV, !nzv))

str(fullSet)

reducedSet = rownames(subset(isNZV, !nzv & freqRatio < floor(nrow(training) / 50)))
reducedSet <- reducedSet[(reducedSet != "allPub") &
                           (reducedSet != "numPeople") &
                           (reducedSet != "Mar") &
                           (reducedSet != "Sun")
                         ]

str(reducedSet)

ctrl <- trainControl(summaryFunction = twoClassSummary, 
                     classProbs = TRUE, savePredictions = TRUE)
set.seed (476)
lrReduced <- train(training[,reducedSet],
                   y = training$Class,
                   method = "glm",
                   metric = "ROC",
                   trControl = ctrl)
lrReduced
head(lrReduced$pred)
confusionMatrix(data = lrReduced$pred$pred, reference = lrReduced$pred$obs)

library(pROC)
reducedRoc <- roc(response = lrReduced$pred$obs, 
                  predictor = lrReduced$pred$successful, 
                  levels = rev(levels(lrReduced$pred$obs))) 
plot(reducedRoc, legacy.axes = TRUE)
auc(reducedRoc)

library(MASS)
set.seed(476)
ldaFit <- train(x = training[, reducedSet],
                y = training$Class,
                method = "lda",
                preProc = c("center","scale"),
                metric = "ROC",
                trControl = ctrl)
ldaFit

ldaTestClasses <- predict(ldaFit, newdata = testing[,reducedSet])
ldaTestProbs <- predict(ldaFit, newdata = testing[,reducedSet], type = "prob")
plot(testing$Class, ldaTestClasses)

set.seed(476)
plsFit <- train(x = training[, reducedSet],
                y = training$Class,
                method = "pls",
                tuneGrid = expand.grid(.ncomp = 1:10),
                preProc = c("center","scale"),
                metric = "ROC",
                trControl = ctrl)
plsFit

plsImpGrant <- varImp(plsFit, scale = FALSE) 
plsImpGrant