if (cos(3.14)>0) print("positive") else
  print ("negative")

isNegative <- cos(3.14) < 0
isNegative

x <- "Predictive Modeling"
typeof(x)

grep("iv", x)
grep("ex", x)


colors <- c("green", "red", "blue", "red", "white");
grep("red", colors)
nchar(colors)

colors2 <- as.factor(colors)
levels(colors2)
as.numeric(colors2)

colors3 <- c("green", "red", "blue", "red", "white", "red", "blue", "red", "white", "red", "blue", "red", "white");
colors4 <- as.factor(colors3)

size <- c(1)
sizes <- c(121, 169, 81, 144)
both <- list(colors = colors2, size = sizes)
both
is.vector(both)
length(both)
names(both)

m <- matrix(1:12, nrow = 3)
m
m[1, 2]
m[1,]
