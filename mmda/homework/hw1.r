# A)

install.packages("AppliedPredictiveModeling")
install.packages("caret")
library(AppliedPredictiveModeling)
library(caret)
data("permeability")

# B)

predictors = fingerprints[, -nearZeroVar(fingerprints)] # 388 left

# C)

trainingRows = createDataPartition(permeability, p = .80, list = FALSE)
trainSet = predictors[trainingRows,]
testSet = predictors[-trainingRows,]

trainSetPreProcess = preProcess(trainSet, c("BoxCox", "center", "scale"))
testSetPreProcess = preProcess(testSet, c("BoxCox", "center", "scale"))

trainSet = predict(trainSetPreProcess, trainSet)
testSet = predict(testSetPreProcess, testSet)

trainY = permeability[trainingRows]
testY = permeability[-trainingRows]

indx = createFolds(trainY, returnTrain = TRUE)
ctrl = trainControl(method = "cv", index = indx)

plsFit = train(x = trainSet, y = trainY,
               method = 'pls',
               tuneGrid = expand.grid(ncomp = 1:100),
               trControl = ctrl)

# 8 latent variables
# R2 = 0.5040

# D)

predicted = predict(plsFit, testSet)
observed = as.vector(testY)
defaultSummary(data.frame(obs = observed, pred = as.vector(predicted)))

# RMSE = 12.4577
# R2 = 0.5770

# E)

lmTrainControl = trainControl(method = "cv", number = 10)
lmFit = train(x = trainSet, y = trainY,
               method = 'lm',
               trControl = lmTrainControl)

predicted = predict(lmFit, testSet)
defaultSummary(data.frame(obs = observed, pred = as.vector(predicted)))

# RMSE = 26.4083
# R2 = 0.2298

pcrFit = train(x = trainSet, y = trainY,
              method = 'pcr',
              tuneGrid = expand.grid(ncomp = 1:100),
              trControl = lmTrainControl)
# ncomp = 32
# R2 = 0.4709

predicted = predict(pcrFit, testSet)
defaultSummary(data.frame(obs = observed, pred = as.vector(predicted)))

# RMSE = 11.7645
# R2 = 0.6357

install.packages("elasticnet")
library(elasticnet)

ridgeFit = train(x = trainSet, y = trainY,
                   method = "ridge",
                   tuneGrid = expand.grid(lambda = seq(0, .05, length = 40)),
                   trControl = ctrl,
                   preProc = c("BoxCox", "center", "scale"))

# lambda = 0.05
# R2 = 0.2737

predicted = predict(ridgeFit, testSet)
defaultSummary(data.frame(obs = observed, pred = as.vector(predicted)))

# RMSE = 13.5403
# R2 = 0.51

enetGrid = expand.grid(lambda = c(0, .01, .05, .1, .15, .2, .25),
                        fraction = seq(.05, 1, length = 20))

enetFit = train(x = trainSet, y = trainY,
                  method = "enet",
                  tuneGrid = enetGrid,
                  trControl = ctrl,
                  preProc = c("BoxCox", "center", "scale"))

# lambda = 0, fraction = 0.25
# R2 = 0.4714

predicted = predict(enetFit, testSet)
defaultSummary(data.frame(obs = observed, pred = as.vector(predicted)))

# RMSE = 1.2785e+20
# R2 = 9.0185e-4

# F)

# The best option in terms of model accuracy is PCR

# I wouldn't recommend any of the created models to replace the permeability
# laboratory experiment due to huge RMSE.
# In my opinion, the training set size should be significantly increased
# in order to get good models.